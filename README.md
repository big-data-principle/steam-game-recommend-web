## 项目描述

#### 基于IofTV-Screen-Vue3二次开发的Steam游戏推荐平台

## 作者介绍

<div align="left" width="250px" style="display: inline-block;">
<img src="assets/icon.png" width="200px">
</div>
<div style="display: inline-block;width: 300px;font-weight: 900">
          <p>卷鑫菜：</p>
          <p>河南工业大学 人工智能与大数据学院 数据科学与大数据专业20级学生</p>
          <p>喜好前端开发,深度学习框架等,曾参与过多个项目的前端开发</p>
          <p>热衷于人工智能，参加人工智能挑战赛获得国奖</p>
</div>

## 技术栈

使用vite+vue3+typescript创建前端项目，使用axios封装网络请求，使用vue-router管理页面路由

使用pinia来做页面状态管理存储

## 前端代码运行

首先先安装依赖 执行 `yarn install`

安装依赖需要node.js环境需要自行下载安装

安装完成后执行:`yarn run dev`

## 项目部署

在云服务器中使用Nginx服务器部署web前端，springBoot部署项目后端，并使用Nginx反向代理将前端请求映射到后端接口
服务器地址：
访问链接：[https://101.43.9.234/](http://101.43.9.234/)进入首页

## 页面展示

### 首页

![image.png](assets/image.png?t=1672852242365)

### 进入游戏页面

![image.png](assets/game.png)
