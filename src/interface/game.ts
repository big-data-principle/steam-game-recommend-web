export interface game {
    appid?:number, //游戏ID
    name?:string, //名称
    headerimage?:string, //游戏封面
    shortdescription?:string, //简短描述
    developer?:string, //开发商
    publisher?:string, //发行商
    genre?:string[], //题材
    tags?:string[], //标签
    categories?:string[], //玩法
    owners?:number[], //拥有数量
    positivereviews?:number, //好评
    negativereviews?:number, //差评
    price?:number, //价格
    initialprice?:number, //初始价格
    discount?:number|string, //折扣
    ccu?:number, //在线人数
    languages?:string[], //语言
    platforms?:string[], //平台
    releasedate?:string, //发布日期
    requireage?:number, //要求年龄
}

