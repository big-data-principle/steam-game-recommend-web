// @ts-ignore
import {GET} from "api/api";

type Keys=string | number

/**
 * 通过id查询游戏
 * @param appid
 */
export const getGameById = (appid:any) => {
    return GET("/getGameById",{appid})
}

/**
 * 联机游戏
 */
export const getMultiPlayerGameList = () => {
    return GET("/getMultiPlayerGameList",{})
}

/**
 * 单人游戏
 */
export const getSinglePlayerGameList = () => {
    return GET("/getSinglePlayerGameList",{})
}

/**
 * 动作类型游戏
 */
export const getActionGameList = () => {
    return GET("/getActionGameList",{})
}

/**
 * 冒险类型游戏
 */
export const getAdventureGameList = () => {
    return GET("/getAdventureGameList",{})
}

/**
 * RPG类型游戏
 */
export const getRPGGameList = () => {
    return GET("/getRPGGameList",{})
}

/**
 * 轮播图游戏
 */
export const getAllGameList = () => {
    return GET("/getAllGameList",{})
}

/**
 * 最近一个月20款
 */
export const getNearMonth = () => {
    return GET("/getNearMonth",{})
}
/**
 * 在线人数前18
 */
export const getCCU = () => {
    return GET("/getCCU",{})
}

/**
 * 年龄分类数量
 */
export const getAgeCount = () => {
    return GET("/getAgeCount",{})
}

/**
 * 最近一个月好评如潮
 */
export const getRateCount = () => {
    return GET("/getRateCount",{})
}

