export enum genreTypes{
    "RPG" = "RPG",
    "动作" = "Action",
    "冒险" = "Adventure",
}
export enum categoriesTypes{
    "多人" = "Multi-player",
    "单人" = "Single-player",
}

export enum GameTypes{
    "多人" = "Multi-player",
    "单人" = "Single-player",
    "RPG" = "RPG",
    "动作" = "Action",
    "冒险" = "Adventure",
}
