import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import type {RouteRecordRaw} from "vue-router"
// @ts-ignore
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    meta:{
      title:'主页',
    },
    redirect:'/index',
    component: () => import('@/views/HomeView.vue'),
    children:[
      {
        path: '/index',
        name: 'index',
        meta:{
          title:'首页',
        },
        component: () => import('@/views/index/index.vue'),
      },
      {
        path:'/game/:id',
        name:'game',
        meta:{
          title:'热门游戏',
        },
        component:()=> import('@/views/game/index.vue'),
      },
    ]
  },
]
const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes,
})

router.beforeEach((to, from, next) => {
  // @ts-ignore
  document.title = to.meta.title;
  next();
})

export default router
